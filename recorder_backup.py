# recorder.py

import Source
from common import sources, uniqwordfile, LOCAL
import sys, os
import random
import os

WORDS_AT_A_TIME = 5

wordindices = []
for si in range(len(sources)):
	wordindices.append(0)

	sources[si].recwords = sources[si].uniqwords[:]
	random.shuffle(sources[si].recwords)
	
sourceindex = 0
# print len(sources)
# print sources[0]

# ensure words directory
if LOCAL:
	try:
		os.mkdir("words")
	except OSError:
		pass

while 1:
	os.system("clear")
	# clear_input()
	print "  ________________________"
	print "  Pressez / Press / Toets"
	print ""
	print "    < ENTER >"
	print ""
	print "  Pour enregistrer 5 mots"
	print "  To record 5 words"
	print "  Om 5 woorden op te nemen"
	print
	input = raw_input("")
	
	if (input == "bye"): break

	for i in range(WORDS_AT_A_TIME):
	
		cursource = sources[sourceindex]
		curwordindex = wordindices[sourceindex]
		
		word = cursource.recwords[curwordindex]
		os.system("clear")
		
		print "  ________________________"
		print ""
		print "  " +  word.decode('latin-1').encode('utf8')
		print "  ________________________"
		print
		
		## PRINT THE "STATUS" BAR
		status = ""
		for x in range(WORDS_AT_A_TIME):
				if x == i:
						status += "[" + str(x+1) + "]"
				else:
						status += " " + str(x+1) + " " 
		print "  " + status
		print
			
		wordfile = uniqwordfile(word)
#		os.system("./recordword \"%s\" > /dev/null 2> /dev/null" % wordfile)
		os.system("./recordword \"%s\"" % wordfile)
	
		# increment word index
		curwordindex += 1
		if (curwordindex >= len(cursource.recwords)):
			curwordindex = 0
		wordindices[sourceindex] = curwordindex
	
		# increment source
		sourceindex += 1
		if (sourceindex >= len(sources)):
			sourceindex = 0
		
		# break



