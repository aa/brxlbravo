# player.py

import Source
from common import sources, randwordfile, wordpath, LOCAL
import os, sys
import random
from time import sleep



sources.reverse()

import json

for srci, src in enumerate(sources):
	os.system("clear")
	words= []
	for word in src.allwords:
		wstr = word.decode('latin-1').encode('utf8')
		wj = {'word': wstr}
		words.append(wj)
		sys.stdout.write(wstr)
		sys.stdout.flush()
		endofline = (word[-1] == "\n")
		if endofline:
			wj['endofline'] = endofline
		word = word.strip().strip("&-:,.?!()[]\"")
		if (len(word) > 0):
			# sys.stdout.write("["+word+"]")
			# sys.stdout.flush()
			wp = os.path.split(wordpath(word))[1]
			wj['path'] = wp
			wf = randwordfile(word, not LOCAL)
			if wf:
				#cmd = "mplayer %s > /dev/null 2> /dev/null" % wf
				cmd = "aplay %s >/dev/null 2>&1" % wf
				# print cmd
				# os.system(cmd)

	of = open("output{}.json".format(srci), "w")
	json.dump(words, of, indent=2)
	of.close()