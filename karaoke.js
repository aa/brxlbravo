function q (sel, root) { return (root || document).querySelector(sel); }
function qq (sel, root) { return Array.from((root || document).querySelectorAll(sel)); }
function sleep (time) { return new Promise(resolve => { window.setTimeout(resolve, time*1000); }) }
function shuffle(array) {
    let counter = array.length;
    while (counter > 0) {
        let index = Math.floor(Math.random() * counter);
        counter--;
        let temp = array[counter];
        array[counter] = array[index];
        array[index] = temp;
    }
    return array;
}
function choice (items) { return items[Math.floor(Math.random()*items.length)]; }
// https://www.sitepoint.com/create-one-time-events-javascript/
function once(node, type, callback) {
    node.addEventListener(type, function(e) {
        e.target.removeEventListener(e.type, arguments.callee);
        return callback(e);
    });
}

async function read_poems (sources, words, display) {
    /*

    read_poems
    perform a list of poems where:

    sources:
        is a list of json files containing the poem sources in the form 
        [ { word: "", path: ""}, ... ]

    words:
        is an iframe DOM element (already loaded) containing links to recordings of the words
        and the link.text matches the path of the sources...
        <a href="url/of/recording.ogg">PATH</a>

    display:
        is the DOM element into which the words will be inserted

    */

    sources = shuffle(qq("a", sources).map(x => x.href));
    let words_index = index_words(words);
    console.log("words_index", words_index);

    let i;
    var sources_parsed = [];
    for (i=0; i<sources.length; i++) {
        let source = await ((await fetch(sources[i])).json());
        console.log(`Parsed poem ${i+1}`, source);
        sources_parsed.push(source);
    }
    console.log(`Parsed ${sources_parsed.length} poems`);
    for (i=0; i<sources_parsed.length; i++) {
        console.log(`Performing poem ${i+1}`);
        await read_poem(sources_parsed[i], words_index, display);
        await sleep(5);
        display.innerHTML = "";
    }
}
function index_words (words) {
    /* return object with word paths as keys, and lists of matching link DOM elements as the values, ie:
        {
            "alle": [ <a href="words/alle_20170301_112211.ogg">, ... ]
            ...
        }
    */
    var links = words.contentDocument.querySelectorAll("a"),
        ret = {};
    for (var li=0; li<links.length; li++) {
        let link = links[li];
        // console.log("words_link", link);
        if (ret[link.textContent] == undefined) {
            ret[link.textContent] = [];
        }
        ret[link.textContent].push(link);
    }
    return ret;
}
async function read_poem (source, words_index, display) {
    /* See read_poems */
    let i=0,
        len = source.length;
    for (; i<len; i++) {
        let word = source[i],
            word_span = document.createElement("span");
        word_span.innerHTML = word.word;
        display.appendChild(word_span);
        await play_audio(audio, choice(words_index[word.path]).href)
    }
}
async function play_audio (audio, href) {
    return new Promise (resolve => {
        once(audio, "canplay", e=> {
            //console.log("canplay");
            audio.play();
            once(audio, "ended", e=> {
                //console.log("ended");
                resolve();
            })
        })
        audio.src = href;        
    })
}

q("#words").addEventListener("load", e=> {
    qq("a", q("#words").contentDocument).forEach(a => {
        a.addEventListener("click", e => {
            console.log("play", a);
            play_audio(q("audio"), a.href);
            e.preventDefault();
        })
    })
    if (!document.body.classList.contains("started")) {
        document.body.classList.add("started");
        read_poems(q("#sources"), q("#words"), q("#display"));
    }
})
