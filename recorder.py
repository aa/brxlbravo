# recorder.py

import Source
from common import sources, uniqwordfile, LOCAL
import sys, os
import random
import os

WORDS_AT_A_TIME = 5

words = {}
for s in sources:
	for w in s.uniqwords:
		if (not words.has_key(w)):
			words[w] = 0
		words[w] += 1
words = words.keys()
random.shuffle(words)
wordindex = 0

# ensure words directory
if LOCAL:
	try:
		os.mkdir("words")
	except OSError:
		pass

while 1:
	os.system("clear")
	# clear_input()
	print "  ________________________"
	print "  Pressez / Press / Toets"
	print ""
	print "    < ENTER >"
	print ""
	print "  Pour enregistrer 5 mots"
	print "  To record 5 words"
	print "  Om 5 woorden op te nemen"
	print
	input = raw_input("")
	
	if (input == "bye"): break

	for i in range(WORDS_AT_A_TIME):
		
		word = words[wordindex]
		os.system("clear")
		
		print "  ________________________"
		print ""
		print "  " +  word.decode('latin-1').encode('utf8')
		print "  ________________________"
		print
		
		## PRINT THE "STATUS" BAR
		status = ""
		for x in range(WORDS_AT_A_TIME):
				if x == i:
						status += "[" + str(x+1) + "]"
				else:
						status += " " + str(x+1) + " " 
		print "  " + status
		print
			
		wordfile = uniqwordfile(word)
		
#		os.system("./recordword \"%s\" > /dev/null 2> /dev/null" % wordfile)
		os.system("./recordword \"%s\"" % wordfile) ## FOR DEBUGGING ##
	
		# increment word index
		wordindex += 1
		if (wordindex >= len(words)):
			random.shuffle(words)
			wordindex = 0



