# player.py

import Source
from common import sources, randwordfile, LOCAL
import os, sys
import random
from time import sleep

if (not LOCAL):
	# ensure words directory
	try:
		os.mkdir("words")
	except OSError:
		pass

sources.reverse()

while 1:
	for src in sources:
		os.system("clear")
		for word in src.allwords:
			sys.stdout.write(word.decode('latin-1').encode('utf8'))
			sys.stdout.flush()
			endofline = (word[-1] == "\n")
			word = word.strip().strip("&-:,.?!()[]\"")
			if (len(word) > 0):
				# sys.stdout.write("["+word+"]")
				# sys.stdout.flush()
				wf = randwordfile(word, not LOCAL)
				if wf:
					#cmd = "mplayer %s > /dev/null 2> /dev/null" % wf
					cmd = "aplay %s >/dev/null 2>&1" % wf
					# print cmd
					os.system(cmd)
				else:
					sleep(0.1)
			if (endofline): sleep(0.75)
		
		sleep(5)
