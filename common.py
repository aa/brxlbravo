# common.py
# -*- coding: latin-1 -*-

from Source import Source
import glob
import string
import os, os.path
import time

# Default LOCAL = False

LOCAL = False
audioextension = "wav" # "wav"
sourcefilespat = "./source/*.txt"
sourcefiles = glob.glob(sourcefilespat)
sources = []
import random

for sf in sourcefiles:
	s = Source()
	s.parsefile(open(sf))
	sources.append(s)
	
ratbl = string.maketrans(
	'�����������������������������������������������ѡ�\'',
	'aaaaaeeeeiiiiooooouuuuconAAAAEEEEIIIIOOOOUUUUCON!?_')

def removeAccents(s):
	"Removes accents from a given string."
	return string.translate(s, ratbl)

def wordpath (w, ensure=True):
	rw = removeAccents(w.strip().lower())
	ret = "words/" + rw
	if ensure and not os.path.exists(ret):
		os.mkdir(ret)
	return ret

def randwordfile (w, ensure=False):
	wp = wordpath(w, ensure)
	try:
		files = os.listdir(wp)
	except OSError:
		return None
	if len(files) > 0:
		return wp + "/" + random.choice(files)
	else:
		return None

def uniqwordfile (w):
	wp = wordpath(w, LOCAL)
	TS = time.strftime("%Y%m%d_%H%M%S")
	w = removeAccents(w)
	return wp+"/"+w+"_"+TS+"."+audioextension
#	
#	files = os.listdir(wp)
#	i = len(files) + 1
#	while (os.path.exists(wp+"/"+w+"_"+str(i)+"."+audioextension)):
#		i += 1
#	return wp+"/"+w+"_"+str(i)+"."+audioextension

