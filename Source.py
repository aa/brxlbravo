#/usr/bin/python
# -*- coding: latin-1 -*-
#

import os.path
import sys, string
import re

# snipped from http://furius.ca/pubcode/pub/conf/common/bin/idify
ratbl = string.maketrans(
	'�����������������������������������������������ѡ�\'',
	'aaaaaeeeeiiiiooooouuuuconAAAAEEEEIIIIOOOOUUUUCON!?_')

def removeAccents(s):
	"Removes accents from a given string."
	return string.translate(s, ratbl)

def linetowords (line):
	ret = []
	line = line.strip()
	words = line.split()
	for word in line.split():
		word = word.lower()
		word = word.strip("&-:,.?!()[]\"")
		# word = removeAccents(word)
		# word = word.encode("ascii", "replace")
		
		if not word: continue
		subwords = word.split("/")
		for sw in subwords:
			ret.append(sw)
	return ret

class Source:
	def __init__(self):
		self.wc = {}
		self.allwords = []
		
	def parsefile(self, f):
		wordsplitter = re.compile(r"(\s+)")
		for l in f.readlines():
			l = l.rstrip()
			for w in wordsplitter.split(l):
				if len(w) == 0: continue
				if (len(w.strip()) == 0):
					self.allwords[-1] += w
				else:
					self.allwords.append(w)
				
				rawword = w.strip().lower().strip("&-:,.?!()[]\"")
				if rawword:
					if (not self.wc.has_key(rawword)):
						self.wc[rawword] = 0
					self.wc[rawword] += 1
			
			self.allwords[-1] += "\n"
			
		self.uniqwords = self.wc.keys()
		self.uniqwords.sort()

#####
# import codecs

if (__name__ == "__main__"):

	import glob
	
	sourcefilespat = "./source/*.txt"
	sourcefiles = glob.glob(sourcefilespat)
	sources = []
	
	for sf in sourcefiles:
		print sf
		s = Source()
		# s.parsefile(codecs.open(sf, "r", "utf-8"))
		s.parsefile(open(sf))
		sources.append(s)
		
		for w in s.uniqwords:
		#	sys.stdout.write("["+w.encode("utf-8", "ignore")+"] ")
			sys.stdout.write("["+w+"] ")
		print
		print
